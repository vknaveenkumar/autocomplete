import React, { Component, useState } from 'react';
import {Select} from './Select'


export default class Form extends Component {

    constructor(props) {
        super(props)
        this.state = {
            userInput: 'chicken',
            cuisineTypes: ['All', 'chinese', 'indian'],
            mealTypes: ['All', "lunch", "dinner", "breakfast", "snack"],
            dishTypes: ['All', 'soup', 'dessert'],
            dietOptions: ['All', 'balanced', 'high-protein', 'high-fiber', 'low-fat', 'low-carb', 'low-sodium'],
            healthOptions: ['All', 'peanut-free', 'tree-nut-free'],
            cusine: 'All',
            meal: 'All',
            dish: 'All',
            diet: 'All',
            health: 'All',
            topSearches: []

        }
        this.fetchApiDetails = debounce(this.fetchApiDetails, 1000);
    }

    componentDidMount() {
        this.fetchApiDetails()
    }

    onChange = (e) => {
        this.setState({
            userInput: e.target.value
        })
        this.fetchApiDetails()
    }

    onChangeType = (name, value) => {
        this.setState({
            [name]: value
        })
        this.fetchApiDetails();
    }

    fetchApiDetails = () => {
        const { searchResult } = this.props
        const { userInput, cusine, meal, dish, diet, health, topSearches } = this.state
        let url = `https://api.edamam.com/search?app_id=d1e0ff4a&app_key=67e60f53112e7a08de15ae6aef11696d`

        if (userInput) url = `${url}&q=${userInput}`
        // if (cusine!=='All') url = `${url}&cuisineType=${cusine}`
        // if (meal!=='All') url = `${url}&mealType=${meal}`
        // if (dish!=='All') url = `${url}&dishType=${dish}`
        if (diet !== 'All') url = `${url}&diet=${diet}`
        //if (health!=='All') url = `${url}&diet=${health}`
        fetch(url).then(res => res.json()).then(response => {
            topSearches.push(userInput)
            if (topSearches.lenght >= 5) {
                topSearches.splice(0, 1)
                this.setState({
                    topSearches
                })
            }
            searchResult(response.hits)
        });
    }

    render() {
        const { userInput, cuisineTypes, mealTypes, dishTypes, dietOptions, healthOptions, topSearches } = this.state
        return (
            <div>
                <div className="search-align">
                    <input
                        type="search"
                        onChange={this.onChange}
                        value={userInput}
                    />
                    <div style={{ display: 'inline' }}>
                        <Select name='cusine' label={'Cusine Type'} options={cuisineTypes} onChangeType={this.onChangeType} />
                    </div>
                    <div style={{ display: 'inline' }}>
                        <Select name='meal' label={'Meal Type'} options={mealTypes} onChangeType={this.onChangeType} />
                    </div>
                    <div style={{ display: 'inline' }}>
                        <Select name='dish' label={'Dish Type'} options={dishTypes} onChangeType={this.onChangeType} />
                    </div>
                    <div style={{ display: 'inline' }}>
                        <Select name='diet' label={'Diet '} options={dietOptions} onChangeType={this.onChangeType} />
                    </div>
                    <div style={{ display: 'inline' }}>
                        <Select name='health' label={'Health'} options={healthOptions} onChangeType={this.onChangeType} />
                    </div>

                </div>
                <div style={{ backgroundColor: 'white', margin: '15px', height: '20px',padding:'10px' }}>Last 5 Search :<span style={{ fontWeight: 'bolder' }}> {topSearches.toString()} </span></div>
            </div>

        )
    }
}





//decbouncing
const debounce = (fn, delay) => {
    let timer = null;
    return function (...args) {
        const context = this;
        timer && clearTimeout(timer);
        timer = setTimeout(() => {
            fn.apply(context, args);
        }, delay);
    };
}
