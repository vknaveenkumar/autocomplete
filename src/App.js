import React, { Component } from 'react';
import Form from './Form'
import {Card} from './Card'
import './App.css';

class App extends Component {

  constructor(props) {
    super(props)
    this.state = {
      searchResult: []
    }
  }


  searchResults = (searchResult) => {
    this.setState({
      searchResult
    })
  }

  render() {
    const { searchResult } = this.state;
    return (
      <div>
        <header>Recipe Search</header>
        <Form searchResult={this.searchResults} />

        <div className="row">
          {searchResult.map((data, i) => {
            return <Card key={i} recipe={data.recipe} />
          })}

        </div>
      </div>
    );
  }
}

export default App;


