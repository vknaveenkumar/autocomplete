import React, { Component } from 'react';

export const Card = ({ recipe }) => {
    return (
      <div className="column">
        <div className="card">
          <h3>{recipe.label}</h3>
          <img src={recipe.image} height={180} alt={'Food Images'}/>
          <p>Diet Labels : <span>{recipe.dietLabels.toString()}</span></p>
          <p>Health Labels :<span>{recipe.healthLabels.toString()}</span></p>
          <p>Calories :<span>{recipe.calories.toString()}</span></p>
        </div>
      </div>
    )
  }