import React, { useState } from 'react';

export const Select = ({ name, label, options, onChangeType }) => {

    const [value, setValue] = useState('All');

    const onChangeValues = (data) => {
        setValue(data.target.value)
        onChangeType(name, data.target.value)
    }

    return (
        <React.Fragment>
            <div>{label}</div>
            <select value={value} onChange={onChangeValues}>
                {options.map((data, i) => {
                    return <React.Fragment>
                        <option key={i} value={data}>{data}</option>
                    </React.Fragment>
                })}
            </select>
        </React.Fragment>
    )
}